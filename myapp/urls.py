from django.contrib import admin
from django.urls import path
from myapp.views import (show_books, create_view, show_book_details, update_view, delete_view)

urlpatterns = [
    path("",show_books, name="show_books" ),
    path("create/", create_view, name="create_book" ),
    path('<int:pk>/', show_book_details, name = "show_book"),
    path('<int:pk>/update_view', update_view, name = 'update_view'),
    path('<int:pk>/delete', delete_view, name = 'delete_view')
]



