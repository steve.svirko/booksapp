from django import forms
from .models import Book


class BookForm(forms.ModelForm):
    
    class Meta:
        model = Book
        fields = ["title","author","book_page",'isbn','in_print', 'date_published']




