# Generated by Django 4.0.6 on 2022-07-18 23:38

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('author', models.CharField(max_length=200)),
                ('book_page', models.IntegerField(null=True)),
                ('url_for_bookcover', models.URLField(blank=True, null=True)),
                ('isbn', models.IntegerField(blank=True, null=True)),
                ('in_print', models.BooleanField(null=True)),
                ('date_published', models.DateField(auto_now=True)),
            ],
        ),
    ]
