from django.db import models

# Create your models here.

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    book_page = models.IntegerField(null=True)
    url_for_bookcover = models.URLField(null=True, blank=True)
    isbn = models.IntegerField(null=True, blank=True)
    in_print = models.BooleanField(null=True)
    date_published = models.DateField(null=True, blank=True)

    def __str__(self): #how exactly to represent an instance of this class as a string "__str__"
        return self.title + "by" + self.author