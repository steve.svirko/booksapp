from turtle import title
from django.shortcuts import redirect, render, get_object_or_404, HttpResponseRedirect
from myapp.models import Book
from myapp.forms import BookForm

# Create your views here.






def show_books(request):
    books = Book.objects.all()
    context = {"booksapp": books,
                "title": "list_of_books"}
    return render(request, "books/list.html", context)


def create_view(request):
    context={}

    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save() 
        return redirect("create_book")
    
    context['form']= form 
    return render(request, "books/create.html", context)


def show_book_details(request, pk):
    book = Book.objects.get(pk=pk)
    context = {"book":book}
    return render(request, "books/detail.html", context)




def update_view(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    form = BookForm(request.POST or None, instance = obj)
    if form.is_valid():
        book = form.save()
        return redirect("show_book", pk = book.pk)
    context["form"] = form
    return render(request, "books/update_view.html", context)





def delete_view(request, pk):
	
	context ={}

	obj = get_object_or_404(Book, pk = pk)


	if request.method == "POST":
		
		obj.delete()
		return redirect("show_books")

	return render(request, "books/delete_view.html", context)


